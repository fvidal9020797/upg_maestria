const assert = require("assert");
const request = require("supertest");
const { app } = require("../server/server");

describe("VERIFICA QUE NO PERMITA INGRESAR VALORES QUE NO SEAN VALIDOS", () => {
    it("TEST OF FIRE POST params:monto", (done) => {
        request(app)
            .post('/abonar')
            .send({ monto: 'SSDSDS' })
            .end((err, response) => {
                assert(response.body.status === false);
                done();
            });
    });
});
describe("VERIFICA QUE NO PUEDE INGRESAR VALORES NEGATIVOS", () => {
    it("TEST OF FIRE POST params:monto", (done) => {
        request(app)
            .post('/abonar')
            .send({ monto: -100 })
            .end((err, response) => {
                assert(response.body.status === false);
                done();
            });
    });
});
describe("VERIFICA QUE TENGA SALDO PARA RETIRAR", () => {
    it("TEST OF FIRE POST params:monto", (done) => {
        request(app)
            .post('/retirar')
            .send({ monto: 500000000 }) // ASUMIENDO QUE EL SALDO EMPIEZA CON 0
            .end((err, response) => {
                assert(response.body.status === false);
                done();
            });
    });
});

describe("VERIFICA SI INCREMENTA EL SALDO ACTUAL LUEGO DE UNA ABONO", () => {
    it("SALDO TOTAL INICIAL ES 0 Y DEBE DAR COMO RESUL TADO 40", (done) => {
        request(app)
            .post('/abonar')
            .send({ monto: 40 })
            .end((err, response) => {
                assert(response.body.saldo === 40);
                done();
            });
    });
});

describe("VERIFICA QUE EL SALDO TOTAL DISMIUYA CUANDO SE REALIZA UN RETIRO", () => {
    it("SALDO TOTAL ES 100 Y SE DECEA RETIRAR 20 ", (done) => {
        request(app)
            .post('/retirar')
            .send({ monto: 20 })
            .end((err, response) => {
                assert(response.body.saldo === 80);

                done();
            });
    });
    it("SALDO TOTAL 100", (done) => {
        request(app)
            .post('/retirar')
            .send({ monto: 10 })
            .end((err, response) => {
                assert(response.body.saldo === 90);
                done();
            });
    });
});