Feature: Mostrar saldo total luego de un abono
 Como client (no humano)
 Quiero ver el saldo total de la cuenta


 Scenario Outline: abono a cuentas
  Given envio valora abonar <number>
    When preparo los datos
    Then verifico el resultado <result>

    Examples:
    | number | result |
    | 30     |    70  | 
