const { Given, When, Then } = require('cucumber')
const { expect } = require('chai')
const httpClient = require('request-promise')

let newUser = {};
let httpOptions = {};
let urbanagerResponse = undefined;

Given('devuelve el saldo total de la cuenta', function() {
    console.log('preparo el contexto');
});
When('preparo los datos para el request', function() {
    httpOptions = {
        method: 'GET',
        uri: 'http://localhost:3000/saldo',
        json: true,
        resolveWithFullResponse: true
    };
});

Then('envio el request', async function() {
    await httpClient(httpOptions)
        .then(function(response) {
            urbanagerResponse = response.body;
        })
        .catch(function(error) {
            urbanagerResponse = error;
        });

});
Then('verifico los datos enviados', function() {
    expect(urbanagerResponse.saldo).to.eql(0);
});