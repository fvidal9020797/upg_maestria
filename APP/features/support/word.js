const { setWorldConstructor } = require("cucumber");

class CustomWorld {
    constructor() {
        this.monto = 111110;
    }

    setMonto(monto) {
        this.monto = monto;
    }

    getMonto() {
        return this.monto
    }
}

setWorldConstructor(CustomWorld);