const { Given, When, Then, setDefaultTimeout } = require('cucumber');
const { expect } = require('chai');
require('chromedriver');
const { Builder, By, Key, until } = require('selenium-webdriver');
let monto = 0;
let chromeDriver = undefined;
Given('dado el monto de {int}', function(int) {
    monto = int;
});
When('navego a la ruta de la UI', async function() {
    chromeDriver = await new Builder().forBrowser('chrome').build();
    await chromeDriver.get('http://localhost:3000');
});
When('lleno el monto', async function() {
    await chromeDriver.findElement(By.id('monto')).sendKeys(monto);
});
When('hacer click en el boton abonar', async function() {
    await chromeDriver.findElement(By.id('send')).click();
});

Then('se debe mostrar el saldo de {int}', async function(result) {
    await chromeDriver.findElement(By.id('result'))
        .getText().then(function(text) {
            showText = parseFloat(text);
        });
    expect(showText).to.eql(result);
    await chromeDriver.quit();
});