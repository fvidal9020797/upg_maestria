const { Given, When, Then } = require('cucumber')
const { expect } = require('chai')
const httpClient = require('request-promise')

let valorRetirar = {};
let httpOptions;
let urbanagerResponse = undefined;
Given('envio valora abonar {int}', function(int) {
    valorRetirar = {
        monto: int
    };
});

When('preparo los datos', function() {
    httpOptions = {
        method: 'POST',
        uri: 'http://localhost:3000/retirar',
        json: true,
        body: valorRetirar,
        resolveWithFullResponse: true
    };
});
Then('verifico el resultado {int}', async function(int) {
    await httpClient(httpOptions)
        .then(function(response) {
            urbanagerResponse = response.body;
        })
        .catch(function(error) {
            urbanagerResponse = error;
        });
    expect(urbanagerResponse.saldo).to.eql(int);
});