//=======================================
// VALIDACIONES PARA ABONAR
//=======================================
const { db } = require('../models/cuenta');
let validarAbono = (req, res, next) => {
    db.saldo = 0;
    if (isNaN(req.body.monto)) {
        return res.status(502).json({
            status: false,
            message: 'debe elegir un monto valido',
            saldo: db.saldo
        });
    }
    if (req.body.monto <= 0) {
        return res.status(502).json({
            status: false,
            message: 'Tiene que abonar un valor positivo',
            saldo: db.saldo
        });
    }
    next();
};
//=======================================
// VALIDACIONES PARA RETIRAR
//=======================================
let validarRetiro = (req, res, next) => {
    db.saldo = 100;
    if (isNaN(req.body.monto)) {
        return res.status(502).json({
            status: false,
            message: 'debe elegir un monto valido',
            saldo: db.saldo
        });
    }
    if (req.body.monto <= 0) {
        return res.status(502).json({
            status: false,
            message: 'Tiene que abonar retirar un monto mayor a 0',
            saldo: db.saldo
        });
    }
    if (parseFloat(db.saldo) - parseFloat(req.body.monto) < 0) {
        return res.status(502).json({
            status: false,
            message: 'No le alcanza para retirar',
            saldo: db.saldo
        });
    }
    next();
};
module.exports = {
    validarAbono,
    validarRetiro
};