const express = require('express');
const app = express();
const bodyParser = require('body-parser'); // procesador de codigo
const path = require('path');
const publicPath = path.resolve(__dirname, 'public');


app.use(express.static(publicPath));
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
    // parse application/json
app.use(bodyParser.json())

app.use(require('./routes/index'));

app.listen(3000);

module.exports = {
    app
}