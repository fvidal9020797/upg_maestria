const express = require('express');
const { validarAbono, validarRetiro } = require('../middlewares/rules_manager');

const { db } = require('../models/cuenta');
const app = express();

app.post('/retirar', [validarRetiro], (req, res) => {
    db.saldo = parseFloat(db.saldo) - parseFloat(req.body.monto);
    res.json({
        status: true,
        saldo: db.saldo
    });
});

app.post('/abonar', [validarAbono], (req, res) => {
    db.saldo = parseFloat(db.saldo) + parseFloat(req.body.monto);
    res.json({
        status: true,
        saldo: db.saldo
    });
});
app.get('/saldo', (req, res) => {
    db.saldo = 0;
    res.json({
        status: true,
        saldo: db.saldo
    });
});

module.exports = app;