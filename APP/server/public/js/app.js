function loadDoc() {
    var xhttp = new XMLHttpRequest();
    let params = 'monto=' + parseFloat(document.getElementById('monto').value);

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let result = JSON.parse(this.responseText);
            document.getElementById("result").innerHTML = result.saldo;
        }
    };
    xhttp.open("POST", "abonar", true);
    xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhttp.send(params);
}